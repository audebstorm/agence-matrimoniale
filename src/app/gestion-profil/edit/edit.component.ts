import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IProfil } from 'src/app/models/IProfil';
import { ProfilsService } from 'src/app/shared/services/profils.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  editForm : FormGroup;
  idRoute! : number;
  constructor(private _fb : FormBuilder, private _ps : ProfilsService, private _router : Router, private _ar : ActivatedRoute){
    this.editForm = this._fb.group({
      nom : [null,[Validators.minLength(2),Validators.maxLength(50),Validators.required]],
      prenom : [null,[Validators.minLength(2),Validators.maxLength(50),Validators.required]],
      ddn : [null,[Validators.required]],
      ville : ["",[Validators.required]],
      homme : [false],
      desc : [null,[Validators.required,Validators.maxLength(500)]]
    })
  }

  ngOnInit(): void {
      this.idRoute = parseInt(this._ar.snapshot.params["id"])
      let profil : IProfil | undefined = this._ps.getById(this.idRoute)
      if(profil){
        this.editForm.patchValue({
          nom : profil.nom,
          prenom : profil.prenom,
          ddn : `${profil.ddn.getFullYear()}-${(profil.ddn.getMonth()+1 < 10)?"0"+(profil.ddn.getMonth()+1):profil.ddn.getMonth()+1}-${(profil.ddn.getDate() < 10)?"0"+profil.ddn.getDate():profil.ddn.getDate()}`,
          ville : profil.ville,
          desc : profil.desc,
          homme : profil.homme
        })
      }
  }

  edit() : void{
    if(this.editForm.valid){
      let newProfil : IProfil = {
        id : this.idRoute,
        nom : this.editForm.value.nom,
        prenom : this.editForm.value.prenom,
        ddn : new Date(this.editForm.value.ddn),
        homme : this.editForm.value.homme,
        ville : this.editForm.value.ville,
        desc : this.editForm.value.desc
      }
    
    this._ps.update(newProfil, this.idRoute)
    this._router.navigateByUrl("/gestion-profil")
    }

    else{
      this.editForm.markAllAsTouched()
    }
  }
}
