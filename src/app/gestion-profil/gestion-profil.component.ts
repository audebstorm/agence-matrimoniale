import { Component } from '@angular/core';
import { IProfil } from '../models/IProfil';
import { ProfilsService } from '../shared/services/profils.service';

@Component({
  selector: 'app-gestion-profil',
  templateUrl: './gestion-profil.component.html',
  styleUrls: ['./gestion-profil.component.scss']

})
export class GestionProfilComponent {
  profils : IProfil[] = []
  constructor(private _profilService : ProfilsService){
    this.profils = this._profilService.getAll()
  }

  deleteProfil(id : number) : void{
    this._profilService.delete(id)
  }
}
