import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IProfil } from 'src/app/models/IProfil';
import { ProfilsService } from 'src/app/shared/services/profils.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {
  ajoutForm : FormGroup;
  constructor(private _fb : FormBuilder, private _ps : ProfilsService, private _router : Router){
    this.ajoutForm = this._fb.group({
      nom : [null,[Validators.minLength(2),Validators.maxLength(50),Validators.required]],
      prenom : [null,[Validators.minLength(2),Validators.maxLength(50),Validators.required]],
      ddn : [null,[Validators.required]],
      ville : ["",[Validators.required]],
      homme : [false],
      desc : [null,[Validators.required,Validators.maxLength(500)]]
    })
  }

  add() : void{
    console.log(this.ajoutForm.value.ddn)
    if(this.ajoutForm.valid){
      let newProfil : IProfil = {
        id : 0,
        nom : this.ajoutForm.value.nom,
        prenom : this.ajoutForm.value.prenom,
        ddn : new Date(this.ajoutForm.value.ddn),
        homme : this.ajoutForm.value.homme,
        ville : this.ajoutForm.value.ville,
        desc : this.ajoutForm.value.desc
      }
    
    this._ps.create(newProfil)
    this._router.navigateByUrl("/gestion-profil")
    }

    else{
      this.ajoutForm.markAllAsTouched()
    }
  }
}
