import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomAgePipe } from './pipes/custom-age.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CustomAgePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CustomAgePipe,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
