import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customAge'
})
export class CustomAgePipe implements PipeTransform {

  transform(value: Date): number  {
    let age : number = new Date().getFullYear() - value.getFullYear()
    return age;
  }

}
