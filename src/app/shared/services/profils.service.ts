import { Injectable } from '@angular/core';
import { IProfil } from 'src/app/models/IProfil';

@Injectable({
  providedIn: 'root'
})
export class ProfilsService {
  private _profils : IProfil [] = [
    {
      id : 1,
      ddn : new Date(1969,3,24),
      homme : true,
      nom : 'Michel',
      prenom : 'Michel',
      desc : 'Je suis un bel homme',
      ville : 'Charleroi'
    },
    {
      id : 2,
      ddn : new Date(1966,0,12),
      homme : false,
      nom : 'Michelle',
      prenom : 'Michelle',
      desc : 'Je suis une belle femme',
      ville : 'Anvers'
    }
  ]
  constructor() { }

  getAll() : IProfil[] {
    return this._profils
  }

  getById(id : number) : IProfil | undefined{
    return this._profils.find(p => p.id === id)
  }

  create(profil : IProfil) : void {
    let newId = Math.max(...this._profils.map(p => p.id))+1
    profil.id = newId
    this._profils.push(profil)
  }

  update(profil : IProfil, id : number) : void {
    this.delete(id)
    this._profils.push(profil)
  }

  delete(id : number) : void {
    this._profils.splice(this._profils.findIndex(p => p.id === id),1)
  }
}
