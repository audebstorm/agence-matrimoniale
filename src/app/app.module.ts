import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AccueilComponent } from './accueil/accueil.component';
import { GestionProfilComponent } from './gestion-profil/gestion-profil.component';
import { MatchComponent } from './match/match.component';
import { SharedModule } from './shared/shared.module';
import { CreateComponent } from './gestion-profil/create/create.component';
import { EditComponent } from './gestion-profil/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AccueilComponent,
    GestionProfilComponent,
    MatchComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
