export interface IProfil{
    id : number
    nom : string
    prenom : string
    ddn : Date
    desc : string
    homme : boolean
    ville : string
}