import { createComponent, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { CreateComponent } from './gestion-profil/create/create.component';
import { EditComponent } from './gestion-profil/edit/edit.component';
import { GestionProfilComponent } from './gestion-profil/gestion-profil.component';
import { MatchComponent } from './match/match.component';

const routes: Routes = [
  {
    path : 'accueil', component : AccueilComponent
  },
  {
    path : 'gestion-profil', component : GestionProfilComponent
  },
  {
    path : 'match', component : MatchComponent
  },
  {
    path : 'create-profil', component : CreateComponent
  },
  {
    path : 'edit-profil/:id', component : EditComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
