import { Component, OnInit } from '@angular/core';
import { IProfil } from '../models/IProfil';
import { ProfilsService } from '../shared/services/profils.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {
  hommes : IProfil[] = []
  femmes : IProfil[] = []
  constructor(private _service : ProfilsService) {}
  ngOnInit(): void {
      let profils : IProfil[] = this._service.getAll()
      this.hommes = profils.filter(p => p.homme)
      this.femmes = profils.filter(p => !p.homme)
  }
}
